package ee.Henn;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class Main {

    public static void main(String[] args) throws Exception {

        SimpleDateFormat f = new SimpleDateFormat("dd-MM-yyyy");
//        Date dt = f.parse("07-03-1955");

//        Calendar cal = new GregorianCalendar();
//        cal.set(1955, Calendar.MARCH, 7);
//        cal.setTime(dt); // see teisendab Date Calendariks
//        dt = cal.getTime(); // see teisendab Calendari Dateks
//        cal.set(Calendar.YEAR, 2019); // see muudab kuupäevas ühe osa

        // selle täienduse tegin kodus ja siin muutsin

        // NB! täna ei ole hea muutuja nimi
        Calendar täna = new GregorianCalendar();
        täna.setTime(new Date());

        String[] nimed = {"Henn", "Ants", "Peeter"}; // selle massiivi võib näiteks hoopis failist lugeda
        Date[] sünnikuupäevad = {
                f.parse("07-03-1955"),
                f.parse("06-08-1982"),
                f.parse("07-01-1960")
        }; // ja selle samamoodi

        // kolmanda massiivi teeme Calendar tüüpi, kuna Calendariga on lihtsam toimetada

        Calendar[] sünnipäevad = new Calendar[sünnikuupäevad.length];
        for(int i = 0; i < sünnikuupäevad.length; i++) {
            sünnipäevad[i] = new GregorianCalendar();
            sünnipäevad[i].setTime(sünnikuupäevad[i]);
            sünnipäevad[i].set(Calendar.YEAR, 2019);
            // Calendar'e ei saa omavahel < > == võrrelda, peab compareTo kasutama
            if (sünnipäevad[i].compareTo(täna) < 1)
            //if (sünnipäevad[i].getTime().getTime() < täna.getTime().getTime())
                sünnipäevad[i].set(Calendar.YEAR, 2020);

        }

        // peale neid operatsioone on kolm massiivi olemas
        for(int i = 0; i < nimed.length; i++) {
            System.out.printf(
                    "%s on sündinud %tB %<te, %<tY ja ta sünnipäev on %tB %<te, %<tY %n",
                    nimed[i], sünnikuupäevad[i], sünnipäevad[i]);
        }


        int kandidaat1 = 0;
        int kandidaat2 = 0;

        for (int i = 1; i < nimed.length; i++) {
            // compareTo kasutamine  kui < 0 siis esimene on <
            //                       kui > 0 siis esimene on >
            //                       kui == 0 siis on võrdsed
            if( sünnikuupäevad[kandidaat1].compareTo(sünnikuupäevad[i]) < 0 ) kandidaat1 = i;
            if (sünnipäevad[kandidaat2].compareTo(sünnipäevad[i]) > 0) kandidaat2 = i;
        }

        System.out.println("Kõige noorem on " + nimed[kandidaat1]);
        System.out.println("Järgmisena peab sünnipäeva " + nimed[kandidaat2]);

//        System.out.printf("%s %tB %<te, %<tY \n", "Henn", dt);
//        System.out.printf("%s %tB %<te, %<tY \n", "Henn", cal);



    }
}
